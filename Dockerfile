FROM busybox:latest
RUN pwd &&\
    mkdir build &&\
    cd build &&\
    touch house.txt &&\
    echo "walls" >> house.txt &&\
    echo "floor" >> house.txt &&\
    ls